# react-native-awesome-package

This is an awesome package!

## Installation

```sh
npm install react-native-awesome-package
```

## Usage

```js
import { multiply } from 'react-native-awesome-package';

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT

---

Made with [create-react-native-library](https://github.com/callstack/react-native-builder-bob)
